import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SignUpFormModel } from './sign-up.model';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {

  public formGroup: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService
  ) { 
    this.formGroup = this.formBuilder.group(new SignUpFormModel());
    this.formGroup.controls.username.setValidators([Validators.required]);
    this.formGroup.controls.email.setValidators([Validators.required, Validators.email]);
    this.formGroup.controls.password.setValidators([Validators.required]);
  }

  ngOnInit(): void {
  }

  submit() {
    this.authService.regist$(this.formGroup.value.email, this.formGroup.value.password, this.formGroup.value.username).subscribe();
  }

}
