import { Injectable } from '@angular/core';
import { of, BehaviorSubject, throwError } from 'rxjs';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { tap, catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
    providedIn: 'root'
  })
export class AuthService {

  private _baseUrl = environment.baseUrl;

  public errorMessage$ = new BehaviorSubject<string>(null);

  public hasAuth$ = new BehaviorSubject<boolean>(null);

  constructor(
    private router: Router,
    protected http: HttpClient,
  ) {}

  public login$(email: string, password: string) {
    const body = {
        email: email, 
        password: password
    };
    return this.http.post(`${this._baseUrl}sessions/create`, JSON.stringify(body), httpOptions).pipe(
        map((response: { id_token: string }) => response.id_token),
        tap(token => this.processLoginResponse(token)),
        catchError(error => {
            this.errorMessage$.next(error.message);
            return throwError(error)
        })
    );
  }

  public regist$(email: string, password: string, username: string) {
    const body = {
        email: email, 
        password: password,
        username: username
    };
    return this.http.post(`${this._baseUrl}users`, JSON.stringify(body), httpOptions).pipe(
        map((response: { id_token: string }) => response.id_token),
        tap(token => this.processLoginResponse(token)),
        catchError(error => {
            this.errorMessage$.next(error.message);
            return throwError(error)
        })
    );
  }

  private processLoginResponse(token: string) {
    localStorage.setItem('access_token', JSON.stringify(token));

    this.setAuth(token);
    this.router.navigateByUrl('/');
  }

  private setAuth(token: string | null) {
    if (token === null) {
      this.hasAuth$.next(false);
    } else {
      this.hasAuth$.next(true);
    }
  }

  public checkAuthForGuard$() {
    if (this.hasAuth$.value === null) {
      const data = localStorage.getItem('access_token');
      const token =  JSON.parse(data);
      this.setAuth(token);
    }
    return of(this.hasAuth$.value);
  }

  public logout$() {
    if (localStorage.getItem('access_token') === null) {
      localStorage.clear();
    } else {
      localStorage.removeItem('access_token');
    }
    this.setAuth(null);
    this.router.navigateByUrl(`/auth/sign-in`);
    return of(true);
  }

  
  
}