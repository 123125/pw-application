import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';

import { SignInFormModel } from './sign-in.model';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {

  public formGroup: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService
  ) { 
    this.formGroup = this.formBuilder.group(new SignInFormModel());
    this.formGroup.controls.email.setValidators([Validators.required, Validators.email]);
    this.formGroup.controls.password.setValidators([Validators.required]);
  }

  ngOnInit(): void {
  }

  submit(e: Event) {
    this.authService.login$(this.formGroup.value.email, this.formGroup.value.password).subscribe();
  }

}
