import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main.component';
import { TransactionsListComponent } from './transactions-list/transactions-list.component';
import { CreateTransactionComponent } from './create-transaction/create-transaction.component';

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      // {
      //   path: '',
      //   component: UserInfoComponent
      // },
      // {
      //   path: 'users',
      //   component: UsersListComponent
      // },
      {
        path: 'transactions',
        component: TransactionsListComponent
      },
      {
        path: 'create-transaction',
        component: CreateTransactionComponent
      }
      // {
      //   path: '**',
      //   redirectTo: 'sign-un',
      //   pathMatch: 'full',
      // }
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
