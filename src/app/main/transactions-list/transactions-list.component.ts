import { Component, OnInit } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { ITransaction } from '../../models/transaction.model';
import { IAppState } from 'src/app/store/state/app.state';
import { Store, select } from '@ngrx/store';
import { selectTransactionsList, selectTransactionStatus } from 'src/app/store/selectors/transactions-list.selector';
import { GetTransactionsList, SortTransactionByDate, SortTransactionByName, SortTransactionByAmount } from 'src/app/store/actions/transactions-list.actions';
import { Router } from '@angular/router';

@Component({
  selector: 'app-transactions-list',
  templateUrl: './transactions-list.component.html',
  styleUrls: ['./transactions-list.component.scss']
})
export class TransactionsListComponent implements OnInit {

  transactionsList$: Observable<ITransaction[]>;
  
  getTransactionsListStatus$ = this._store.pipe(select(selectTransactionStatus));

  onDestroy$ = new Subject();

  constructor( private _store: Store<IAppState>, private _router: Router) { }

  ngOnInit(): void {
    this._store.dispatch(new GetTransactionsList());
    this.transactionsList$ = this._store.pipe(
      select(selectTransactionsList)
    ); 
    // this.mainService.getTransactionsList$().pipe(
    //   map((response: any) => response.trans_token),
    //   takeUntil(this.onDestroy$)
    //   ).subscribe((response: ITransaction[]) => this.transactionsList = response );
  }

  repeatTransaction(transaction: ITransaction) {
    this._router.navigate(['/create-transaction'],  {
      queryParams:{
          'username': transaction.username, 
          'amount': Math.abs(transaction.amount)
      }
    })
  }

  ngOnDestroy() {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

  sortByDate() {
  }

  sortTransactions(type: string) {
    console.log(type)
    switch(type){
      case 'dateUp': this._store.dispatch(new SortTransactionByDate('up'));
      break;
      
      case 'dateDown': this._store.dispatch(new SortTransactionByDate('down'));
      break;

      case 'nameUp': this._store.dispatch(new SortTransactionByName('up'));
      break;
      
      case 'nameDown': this._store.dispatch(new SortTransactionByName('down'));
      break;

      case 'amountUp': this._store.dispatch(new SortTransactionByAmount('up'));
      break;
      
      case 'amountDown': this._store.dispatch(new SortTransactionByAmount('down'));
      break;

      default: return;
    }
  }

}
