import { Component, OnInit } from '@angular/core';
import { Subject, Observable } from 'rxjs';

import { MainService } from '../main.service';
import { IUserInfo } from '../../models/user-info.model';
import { Store, select } from '@ngrx/store';
import { IAppState } from 'src/app/store/state/app.state';
import { GetUserInfo } from 'src/app/store/actions/user-info.actions';
import { selectUserInfo } from 'src/app/store/selectors/user-info.selector';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.scss']
})
export class UserInfoComponent implements OnInit {

  userInfo$: Observable<IUserInfo> = this._store.pipe(select(selectUserInfo));

  onDestroy$ = new Subject();

  constructor(
    private mainService: MainService,
    private _store: Store<IAppState>
    ) { }

  ngOnInit(): void {
    this._store.dispatch(new GetUserInfo())
    // this.mainService.getUserInfo$().pipe(
    //   map((response: any) => response.user_info_token),
    //   takeUntil(this.onDestroy$)
    //   ).subscribe(user_info_token => this.userInfo = user_info_token);
  }

  ngOnDestroy() {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

}
