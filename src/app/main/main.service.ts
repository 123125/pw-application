import { Injectable } from '@angular/core';
import { of, BehaviorSubject, throwError } from 'rxjs';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { tap, catchError, map, delay } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
    providedIn: 'root'
  })
export class MainService {

  private _baseUrl = environment.baseUrl;

  public errorMessage$ = new BehaviorSubject<string>(null);

  
  constructor(
    private router: Router,
    protected http: HttpClient,
  ) {}

  public getUserInfo$() {
    return this.http.get(`${this._baseUrl}api/protected/user-info`, httpOptions).pipe(
      catchError(error => {
        this.errorMessage$.next(error.message);
        return throwError(error)
      })
    );
  }

  public getUsersList$(filter: string = '') {
    let body = {
      filter: filter
    }

    return this.http.post(`${this._baseUrl}api/protected/users/list`, JSON.stringify(body), httpOptions).pipe(
      catchError(error => {
        this.errorMessage$.next(error.message);
        return throwError(error)
      })
    );
  }

  public getTransactionsList$() {
    return this.http.get(`${this._baseUrl}api/protected/transactions`, httpOptions).pipe(
      catchError(error => {
        this.errorMessage$.next(error.message);
        return throwError(error)
      })
    );
  }

  public createTransaction$(name: string, amount: number) {
    let body = {
      name: name,
      amount: amount
    }

    return this.http.post(`${this._baseUrl}api/protected/transactions`, JSON.stringify(body), httpOptions).pipe(
      catchError(error => {
        this.errorMessage$.next(error.message);
        return throwError(error)
      })
    );
  }
  
}