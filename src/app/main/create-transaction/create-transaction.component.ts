import { Component, OnInit } from '@angular/core';
import { map, takeUntil, switchMap } from 'rxjs/operators';
import { Subject } from 'rxjs';

import { MainService } from '../main.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IAppState } from 'src/app/store/state/app.state';
import { Store, select } from '@ngrx/store';
import { NewTransaction } from 'src/app/store/actions/transactions-list.actions';
import { selectUserInfo } from 'src/app/store/selectors/user-info.selector';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-create-transaction',
  templateUrl: './create-transaction.component.html',
  styleUrls: ['./create-transaction.component.scss']
})
export class CreateTransactionComponent implements OnInit {

  public formGroup: FormGroup;

  onDestroy$ = new Subject();
  onSearch$ = new Subject();

  constructor(
    private mainService: MainService, 
    private formBuilder: FormBuilder,
    private _store: Store<IAppState>,
    private _activatedRoute: ActivatedRoute
  ) {
    this.formGroup = this.formBuilder.group({name: null, amount: null});
    this.formGroup.controls.name.setValidators([Validators.required]);
    this.formGroup.controls.amount.setValidators([Validators.required]);

    this._activatedRoute.queryParams.pipe(
      takeUntil(this.onDestroy$)
    ).subscribe(queryParams => {
      if (queryParams.username) this.formGroup.controls.name.setValue(queryParams.username);
      if (queryParams.amount) this.formGroup.controls.amount.setValue(queryParams.amount);
    })
  }

  ngOnInit(): void {
  }

  ngOnDestroy() {
    this.onDestroy$.next();
    this.onDestroy$.complete();
    this.onSearch$.complete();
  }

  submit() {
    this._store.pipe(
      select(selectUserInfo),
      map(userInfo => userInfo.balance),
      takeUntil(this.onSearch$),
      takeUntil(this.onDestroy$),
      ).subscribe(balance => {
        this.onSearch$.next();
        if (balance > this.formGroup.value.amount) {
          this._store.dispatch(new NewTransaction({name: this.formGroup.value.name, amount: this.formGroup.value.amount}))
        } 
        else {
          alert('Sorry, you don\'t have emough money!')
        }
      })
    // this.mainService.createTransaction$(this.formGroup.value.name, this.formGroup.value.amount).subscribe()
    
  }

}
