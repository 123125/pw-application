import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter, Input } from '@angular/core';
import { map, takeUntil, switchMap, debounceTime, distinctUntilChanged, takeWhile, tap, catchError } from 'rxjs/operators';
import { Subject, BehaviorSubject, timer, throwError, of, Observable } from 'rxjs';

import { MainService } from '../main.service';
import { IAppState } from 'src/app/store/state/app.state';
import { Store, select } from '@ngrx/store';
import { GetUsersList } from 'src/app/store/actions/users-list.actions';
import { selectUsersList } from 'src/app/store/selectors/users-list.selector';


export interface User {
  id: number,
  name: string
}

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit {

  @Output() getSelectUser = new EventEmitter<User>();

  @Input() selectedUser: User;

  usersList$: Observable<User[]>;
  

  getUsersListStatus$ = new BehaviorSubject<string>('');

  onDestroy$ = new Subject();

  constructor(private mainService: MainService, private _store: Store<IAppState>) { }

  ngOnInit(): void {}

  ngOnDestroy() {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

  getUsersList(filter: string) {
    this.onDestroy$.next();
    timer(300).pipe(
      takeUntil(this.onDestroy$),
      tap(() => {
        if (filter === '') this.usersList$ = of([]);
      }),
      takeWhile(() => filter !== ''),
      tap(() => this.getUsersListStatus$.next('pending')),
      switchMap(() => {
        this._store.dispatch(new GetUsersList(filter));
        this.usersList$ = this._store.pipe(select(selectUsersList))
        this.getUsersListStatus$.next('success')
        return of();
      }),
      catchError(error => {
        this.getUsersListStatus$.next('error');
        this.usersList$ = of([]);
        return throwError(error);
      }),
    ).subscribe();
  }

  selectUser(user: User) {
    this.selectedUser = user;
    this.getSelectUser.emit(user);
  }
  
}
