import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AuthService } from '../auth/auth.service';

@Injectable()
export class HttpInterceptorService implements HttpInterceptor {
  constructor(
    private authService: AuthService
  ) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {

    // const token = this.browserStorage.getFromLocalStorage(EStorageKeys.TOKEN);
    const data = localStorage.getItem('access_token');
    let token = JSON.parse(data);
    const authHeader = token ? {Authorization: `Bearer ${token}`} : {};

    const request = req.clone({
      setHeaders: {
        ...authHeader
      }
    });

    return next.handle(request).pipe(
      tap(
        event => {},
        err => {
          if (err instanceof HttpErrorResponse && err.status === 401) {
            console.warn('Unauthorized');
            // this.authService.logout$();
          }
        }
      )
    );
  }
}
