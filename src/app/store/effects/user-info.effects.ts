import { Injectable } from "@angular/core";
import { Store } from "@ngrx/store";
import { Effect, ofType, Actions } from '@ngrx/effects';
import { IAppState } from "../state/app.state";
import { EUserInfoActions, GetUserInfo, GetUserInfoSuccess } from "../actions/user-info.actions";
import { switchMap, map, catchError } from 'rxjs/operators'
import { of, throwError } from "rxjs";
import { MainService } from "../../main/main.service";
import { IUserInfo } from "../../models/user-info.model";

@Injectable()
export class UserInfoEffects {
    @Effect()
    getUserInfo$ = this._actions$.pipe(
        ofType<GetUserInfo>(EUserInfoActions.GetUserInfo),
        switchMap( () => this.mainService.getUserInfo$() ),
        map( (response: any) => response.user_info_token ),
        switchMap((userInfo: IUserInfo) => {
            return of(new GetUserInfoSuccess(userInfo));
        }),
        catchError(error => throwError(error))
    );

    constructor(
        private mainService: MainService,
        private _actions$: Actions,
    ) { }
}