import { Injectable } from "@angular/core";
import { Effect, ofType, Actions } from '@ngrx/effects';
import { ETransactionsListActions, GetTransactionsList, GetTransactionsListSuccess, NewTransaction, NewTransactionSuccess, SetTransactionStatus } from "../actions/transactions-list.actions";
import { switchMap, map, catchError, concatMap, finalize, concat, tap } from 'rxjs/operators'
import { of, throwError, BehaviorSubject } from "rxjs";
import { MainService } from "../../main/main.service";
import { ITransaction } from "../../models/transaction.model";
import { IAppState } from '../state/app.state';
import { Store } from '@ngrx/store';
import { GetUserInfo } from '../actions/user-info.actions';

@Injectable()
export class TransactionsListEffects {
    @Effect()
    getTransactionsList$ = this._actions$.pipe(
        ofType<GetTransactionsList>(ETransactionsListActions.GetTransactionsList),
        tap(() => this._store.dispatch(new SetTransactionStatus('pending'))),
        switchMap( () => {
            return this.mainService.getTransactionsList$()
        }),
        map( (response: any) => response.trans_token ),
        switchMap((list: ITransaction[]) => {
            this._store.dispatch(new SetTransactionStatus('success'));
            return of(new GetTransactionsListSuccess(list));
        }),
        catchError(error => {
            this._store.dispatch(new SetTransactionStatus('error'));
            return throwError(error);
        })
    );

    @Effect()
    newTransaction$ = this._actions$.pipe(
        ofType<NewTransaction>(ETransactionsListActions.NewTransaction),
        map(action => action.payload),
        switchMap( item => {
            return this.mainService.createTransaction$(item.name, item.amount)
        }),
        map( (response: any) => response.trans_token ),
        switchMap((list: ITransaction) => {
            return of(new NewTransactionSuccess(list));
        }),
        concatMap(() => {
            console.log(1111)
            this._store.dispatch(new GetUserInfo())
            return of();
         } ),
        catchError(error => throwError(error))
    );

    constructor(
        private mainService: MainService,
        private _actions$: Actions,
        private _store: Store<IAppState>
    ) { }
}