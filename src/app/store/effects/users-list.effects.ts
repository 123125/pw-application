import { Injectable } from "@angular/core";
import { Store } from "@ngrx/store";
import { Effect, ofType, Actions } from '@ngrx/effects';
import { IAppState } from "../state/app.state";
import { EUsersListActions, GetUsersList, GetUsersListSuccess } from "../actions/users-list.actions";
import { switchMap, map, catchError } from 'rxjs/operators'
import { of, throwError } from "rxjs";
import { MainService } from "../../main/main.service";
import { IUser } from "../../models/user.model";

@Injectable()
export class UsersListEffects {
    @Effect()
    getUsersList$ = this._actions$.pipe(
        ofType<GetUsersList>(EUsersListActions.GetUsersList),
        map(action => action.payload),
        switchMap( string => this.mainService.getUsersList$(string) ),
        switchMap((list: IUser[]) => {
            return of(new GetUsersListSuccess(list));
        }),
        catchError(error => throwError(error))
    );

    constructor(
        private mainService: MainService,
        private _actions$: Actions,
    ) { }
}