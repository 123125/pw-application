import { Action } from "@ngrx/store";
import { IUserInfo } from "../../models/user-info.model";


export enum EUserInfoActions {
    GetUserInfo = '[User Info] Get User Info',
    GetUserInfoSuccess = '[User Info] Get User Info Success',
}

export class GetUserInfo implements Action {
    public readonly type = EUserInfoActions.GetUserInfo;
}

export class GetUserInfoSuccess implements Action {
    public readonly type = EUserInfoActions.GetUserInfoSuccess;
    constructor(public payload: IUserInfo) {}
}

export type UserInfoActions = GetUserInfo | GetUserInfoSuccess;