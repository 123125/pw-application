import { Action } from "@ngrx/store";
import { IUser } from "../../models/user.model";


export enum EUsersListActions {
    GetUsersList = '[Users List] Get Users List',
    GetUsersListSuccess = '[Users List] Get Users List Success',
}

export class GetUsersList implements Action {
    public readonly type = EUsersListActions.GetUsersList;
    constructor(public payload: string) {}
}

export class GetUsersListSuccess implements Action {
    public readonly type = EUsersListActions.GetUsersListSuccess;
    constructor(public payload: IUser[]) {}
}

export type UsersListActions = GetUsersList | GetUsersListSuccess;