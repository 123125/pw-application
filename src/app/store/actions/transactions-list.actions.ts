import { Action } from "@ngrx/store";
import { ITransaction } from 'src/app/models/transaction.model';

export enum ETransactionsListActions {
    GetTransactionsList = '[Transactions List] Get Transactions List',
    GetTransactionsListSuccess = '[Transactions List] Get Transactions List Success',
    NewTransaction = '[Transactions List] New Transaction',
    NewTransactionSuccess = '[Transactions List] New Transaction Success',
    SetTransactionStatus = '[Transactions List] Set Transaction Status',
    SortTransactionByDate = '[Transactions List] Sort Transactions By Date',
    SortTransactionByName = '[Transactions List] Sort Transactions By Name',
    SortTransactionByAmount = '[Transactions List] Sort Transactions By Amount',
}

export class GetTransactionsList implements Action {
    public readonly type = ETransactionsListActions.GetTransactionsList;
}

export class GetTransactionsListSuccess implements Action {
    public readonly type = ETransactionsListActions.GetTransactionsListSuccess;
    constructor(public payload: ITransaction[]) {}
}

export class NewTransaction implements Action {
    public readonly type = ETransactionsListActions.NewTransaction;
    constructor(public payload: {amount: number, name: string}) {}
}

export class NewTransactionSuccess implements Action {
    public readonly type = ETransactionsListActions.NewTransactionSuccess;
    constructor(public payload: ITransaction) {}
}

export class SetTransactionStatus implements Action {
    public readonly type = ETransactionsListActions.SetTransactionStatus;
    constructor(public payload: string) {}
}

export class SortTransactionByDate implements Action {
    public readonly type = ETransactionsListActions.SortTransactionByDate;
    constructor(public payload: string) {}
}

export class SortTransactionByName implements Action {
    public readonly type = ETransactionsListActions.SortTransactionByName;
    constructor(public payload: string) {}
}

export class SortTransactionByAmount implements Action {
    public readonly type = ETransactionsListActions.SortTransactionByAmount;
    constructor(public payload: string) {}
}

export type TransactionsListActions = GetTransactionsList | GetTransactionsListSuccess | NewTransaction | NewTransactionSuccess 
                                    | SetTransactionStatus | SortTransactionByDate | SortTransactionByName | SortTransactionByAmount;