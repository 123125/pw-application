import { initialUsersListState } from "../state/users-list.state";
import { UsersListActions, EUsersListActions } from "../actions/users-list.actions";
import { IUser } from "../../models/user.model";


export const usersListReducers = (
    state = initialUsersListState,
    action: UsersListActions
    ): IUser[] => {
        switch (action.type) {

            case EUsersListActions.GetUsersListSuccess: {
                return [ ...action.payload ];
            }

            default:
                return state;
        }
    }