import { ActionReducerMap } from "@ngrx/store";
import { IAppState } from "../state/app.state";
import { routerReducer } from "@ngrx/router-store";
import { userInfoReducers } from "./user-info.reducers";
import { transactionsListReducers } from "./transactions-list.reducers";
import { usersListReducers } from "./users-list.reducers";

export const appReducers: ActionReducerMap<IAppState, any> = {
    router: routerReducer,
    userInfo: userInfoReducers,
    transactionsList: transactionsListReducers,
    usersList: usersListReducers
}