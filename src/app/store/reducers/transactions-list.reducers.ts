import { TransactionsListActions, ETransactionsListActions } from "../actions/transactions-list.actions";
import { initialTransactionsListState, ITransactionsListState } from '../state/transactions-list.state';

export const transactionsListReducers = (
    state = initialTransactionsListState,
    action: TransactionsListActions
): ITransactionsListState => {
    switch (action.type) {
        case ETransactionsListActions.GetTransactionsListSuccess: {
            return {
                ...state,
                transactions: action.payload
            }
        }

        case ETransactionsListActions.NewTransactionSuccess: {
            return {
                ...state,
                transactions: [action.payload, ...state.transactions]
            }
        }

        case ETransactionsListActions.SetTransactionStatus: {
            return {
                ...state,
                getTransactionsListStatus: action.payload
            }
        }

        case ETransactionsListActions.SortTransactionByDate: {
            console.log(action.payload)
            return {
                ...state,
                transactions: [...state.transactions].sort((a, b) => {
                    const dateA: any = new Date(a.date), dateB: any = new Date(b.date);
                    return doSorting(action, dateA, dateB);
                })
            }
        }

        case ETransactionsListActions.SortTransactionByName: {
            console.log(action.payload)
            return {
                ...state,
                transactions: [...state.transactions].sort((a, b) => {
                    const nameA: any = a.username.toLowerCase(), nameB: any = b.username.toLowerCase();
                    return doSorting(action, nameA, nameB);
                })
            }
        }

        case ETransactionsListActions.SortTransactionByAmount: {
            console.log(action.payload)
            return {
                ...state,
                transactions: [...state.transactions].sort((a, b) => {
                    const amountA: any = Math.abs(a.amount), amountB: any = Math.abs(b.amount);
                    return doSorting(action, amountA, amountB);
                })
            }
        }

        default:
            return state;
    }
}

function doSorting(action, optA, optB) {
    if (action.payload === 'up') {
        return optA - optB;
    } else if (action.payload === 'down') {
        return optB - optA;
    } else {
        return;
    }
}