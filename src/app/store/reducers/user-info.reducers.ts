import { initialUserInfoState } from "../state/user-info.state";
import { UserInfoActions, EUserInfoActions } from "../actions/user-info.actions";
import { IUserInfo } from "../../models/user-info.model";


export const userInfoReducers = (
    state = initialUserInfoState,
    action: UserInfoActions
    ): IUserInfo => {
        switch (action.type) {
            case EUserInfoActions.GetUserInfoSuccess: {
                return {
                    ...state,
                    ...action.payload
                };
            }

            default:
                return state;
        }
    }