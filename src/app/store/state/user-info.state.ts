import { IUserInfo } from "../../models/user-info.model";

export const initialUserInfoState: IUserInfo = {
    balance: null,
    email: null,
    id: null,
    name: null
}
