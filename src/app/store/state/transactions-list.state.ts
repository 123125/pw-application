import { ITransaction } from "../../models/transaction.model";

export interface ITransactionsListState {
    transactions: ITransaction[];
    getTransactionsListStatus: string;
}

export const initialTransactionsListState = {
    transactions: [],
    getTransactionsListStatus: ''
}


