import { RouterReducerState } from '@ngrx/router-store';
import { initialUserInfoState } from './user-info.state';
import { initialTransactionsListState, ITransactionsListState } from './transactions-list.state';
import { initialUsersListState } from './users-list.state';
import { IUserInfo } from "../../models/user-info.model";
import { IUser } from "../../models/user.model";


export interface IAppState {
    router?: RouterReducerState;
    userInfo: IUserInfo;
    transactionsList: ITransactionsListState,
    usersList: IUser[]
}

export const initialAppState: IAppState = {
    userInfo: initialUserInfoState,
    transactionsList: initialTransactionsListState,
    usersList: initialUsersListState
}

export function getInitialState(): IAppState {
    return initialAppState;
}