import { createSelector, State } from "@ngrx/store";
import { IAppState } from "../state/app.state";
import { IUser } from "../../models/user.model";

const usersListState = (state: IAppState) => state.usersList;

export const selectUsersList = createSelector(
    usersListState,
    (state: IUser[]) => state
);
