import { createSelector, State } from "@ngrx/store";
import { IAppState } from "../state/app.state";
import { IUserInfo } from "../../models/user-info.model";

const userInfoState = (state: IAppState) => state.userInfo;

export const selectUserInfo = createSelector(
    userInfoState,
    (state: IUserInfo) => state
);
