import { createSelector, State } from "@ngrx/store";
import { IAppState } from "../state/app.state";
import { ITransaction } from "../../models/transaction.model";

const transactionsListState = (state: IAppState) => state.transactionsList.transactions;
const transactionStatusState = (state: IAppState) => state.transactionsList.getTransactionsListStatus;

export const selectTransactionsList = createSelector(
    transactionsListState,
    (state: ITransaction[]) => state
);

export const selectTransactionStatus = createSelector(
    transactionStatusState,
    (state: string) => state
);